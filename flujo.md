# Flujo de Trabajo

Un flujo de trabajo típico incluye:

1. **Creación del Repositorio:** Iniciar un repositorio para el proyecto.

2. **Desarrollo en Branch:** Trabajar en una rama separada para evitar cambios directos en la rama principal.

3. **Commit y Push:** Realizar cambios en el código, hacer commits y enviarlos al repositorio remoto.

4. **Pull Request:** Proponer cambios a la rama principal mediante un "Pull Request" en GitLab.

5. **Revisión y Comentarios:** Otros desarrolladores revisan y comentan los cambios propuestos.

6. **Merge:** Una vez aprobados, los cambios se fusionan con la rama principal.