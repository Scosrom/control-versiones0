# Git y GitLab

### Git

Git es un SCV distribuido que permite el seguimiento de cambios, colaboración y gestión eficiente de proyectos.

### GitLab

GitLab es una plataforma de desarrollo de software que proporciona servicios de gestión de repositorios Git, integración continua y seguimiento de problemas.