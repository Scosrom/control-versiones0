# Documentación Técnica sobre Sistemas de Control de Versiones

![imagen](/img/cv.jpg)

## ÍNDICE

1. [Introducción](intro.md)
2. [Conceptos Básicos](conceptosBasicos.md)
3. [Git y GitLab](git.md)
4. [Flujo de Trabajo](flujo.md)
5. [Configuración y Uso Básico](configuracion.md)
6. [Ramificación](ramificacion.md)
7. [Colaboración](colaboracion.md)
8. Referencias

    [Documentación oficial de Git](https://git-scm.com/doc)

    [Documentación oficial de GitLab](https://docs.gitlab.com)
    
    [Pro Git Book](https://git-scm.com/book/en/v2)

9. Licencia

    ![Alt text](image.png)

