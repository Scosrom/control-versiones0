# Ramificación

Crear una Rama

```
git checkout -b nombre_de_la_rama

```

Cambiar de Rama

```
git checkout nombre_de_la_rama

```

Eliminar una Rama

```
git branch -d nombre_de_la_rama

```