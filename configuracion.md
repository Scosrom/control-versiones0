# Configuración y Uso Básico

Configuración de Git

```
git config --global user.name "Tu Nombre"
git config --global user.email "tu@email.com"

```

Clonar un Repositorio

```
git clone URL_del_repositorio

```

Commit y Push

```
git add archivo_modificado
git commit -m "Mensaje descriptivo"
git push origin nombre_de_la_rama

```