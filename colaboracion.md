# Colaboración

Pull Request en GitLab

1. Navegar al repositorio en GitLab.

2. Seleccionar "Merge Request" o "Pull Request".

3. Seleccionar las ramas de origen y destino.

4. Describir los cambios y solicitar la revisión de otros colaboradores.

5. Completar la solicitud y esperar la revisión.