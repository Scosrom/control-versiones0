# Conceptos Básicos

### Repositorio
Un repositorio es un espacio donde se almacena el código fuente y sus historiales de cambios.

### Commit
Un commit representa un conjunto de cambios en el código fuente y contiene un mensaje descriptivo.

### Branch
Una rama es una línea independiente de desarrollo que no afecta al código principal hasta que se fusiona.

### Merge
La fusión (merge) combina los cambios de una rama en otra.